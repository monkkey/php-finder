<?php

namespace Monkkey\Tools\Finder;

interface NeedleInterface
{
    /**
     * Set the property to search by
     *
     * @param mixed $key
     * @return NeedleInterface
     */
    public function by($key): NeedleInterface;

    /**
     * Search in the provided objects
     *
     * @param object<T>[] $objects
     * @return bool
     */
    public function in(array $objects): bool;
}
