<?php

namespace Monkkey\Tools\Finder;

final class ItemFinder
{
    /**
     * Returns the first match in the collection based on a callback.
     * If there is no match, null is returned.
     *
     * @param array    $collection The array of items to search in.
     * @param callable $callback   Called on every item of the collection. Must return a boolean.
     * @return mixed               The first match or null if there is not match.
     */
    public function findFirst(array $collection, callable $callback)
    {
        $reduceToMatchingItem = function (callable $callback) : \Closure {
            /**
             * @param  mixed $match
             * @param  mixed $item
             * @return mixed
             */
            return function ($match, $item) use ($callback) {
                if (null !== $match) {
                    return $match;
                }
                if ($callback($item)) {
                    return $item;
                }
                return $match;
            };
        };

        return array_reduce(
            $collection,
            $reduceToMatchingItem($callback),
            null
        );
    }

    /**
     * Returns the matches in the collection based on a callback.
     * If there is no match, an empty array is returned.
     *
     * @param  array    $collection The array of items to search in.
     * @param  callable $callback   Called on every item of the collection. Must return a boolean.
     * @return array                The matches array or an empty array if there is not match.
     */
    public function findMany(array $collection, callable $callback)
    {
        $reduceToMatchingItems = function (callable $callback) : \Closure {
            /**
             * @param  array $matches
             * @param  mixed $item
             * @return mixed
             */
            return function (array $matches, $item) use ($callback) {
                if ($callback($item)) {
                    $matches[] = $item;
                }
                return $matches;
            };
        };

        return array_reduce(
            $collection,
            $reduceToMatchingItems($callback),
            []
        );
    }
}
