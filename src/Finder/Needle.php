<?php

namespace Monkkey\Tools\Finder;

use Monkkey\Tools\Finder\Exception\NeedleException;

final class Needle implements NeedleInterface
{
    /**
     * @var mixed
     */
    private $object;

    /**
     * @var string
     */
    private $classname;

    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $getter;

    /**
     * @var bool
     */
    private $isPublic = false;

    /**
     * @param object $object
     */
    public function __construct(object $object)
    {
        $this->object = $object;
        $this->classname = get_class($object);
    }

    /**
     * {@inheritDoc}
     * @param  string          $property
     * @throws NeedleException
     */
    public function by($property): NeedleInterface
    {
        if (!property_exists($this->object, $property)) {
            throw new NeedleException("The property {$property} does not exist in class {$this->classname}");
        }

        $this->property = $property;

        $reflectionProperty = new \ReflectionProperty($this->object, $property);

        if ($reflectionProperty->isPublic()) {
            $this->isPublic = true;
            return $this;
        }

        $capitalizedProperty = ucfirst($property);
        $resolvedGetter = "get{$capitalizedProperty}";

        if (!method_exists($this->object, $resolvedGetter)) {
            $message = "The getter {$resolvedGetter} does not exist on this object.";
            $message .= " Set the property as public or implement the method {$resolvedGetter}";
            throw new NeedleException($message);
        }

        $this->getter = $resolvedGetter;

        return $this;
    }

    /**
     * {@inheritDoc}
     * @throws NeedleException
     */
    public function in(array $objects): bool
    {
        $isNullPublicProperty = $this->isPublic && null === $this->property;
        $isNullGetter = null === $this->getter;

        if ($isNullPublicProperty && $isNullGetter) {
            throw new NeedleException("The property is not set. Call 'by' before 'in'.");
        }
        
        if ($this->isPublic) {
            $compareFn = function (object $object) {
                return $object->{$this->property} === $this->object->{$this->property};
            };
        } else {
            $compareFn = function (object $object) {
                return $object->{$this->getter}() === $this->object->{$this->getter}();
            };
        }

        return array_reduce($objects, $this->toObjectFound($compareFn), false);
    }

    /**
     * @param callable $compareFn
     * @return \Closure
     */
    private function toObjectFound(callable $compareFn)
    {
        /**
         * @param  boolean $found
         * @param  object  $object
         * @return boolean
         */
        return function (bool $found, Object $object) use ($compareFn): bool {
            return $found || $compareFn($object);
        };
    }
}
