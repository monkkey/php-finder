<?php

namespace Monkkey\Tools\Finder;

final class ObjectFinder implements ObjectFinderInterface
{
    /**
     * {@inheritDoc}
     */
    public function search(object $object): NeedleInterface
    {
        return new Needle($object);
    }
}
