<?php

namespace Monkkey\Tools\Hydrator\Exception;

final class ObjectHydratorException extends \LogicException
{
}
