<?php

namespace Monkkey\Tools\Sorter;

interface SorterInterface
{
    /**
     * Undocumented function
     *
     * @param T[] $data
     * @param string $key
     * @param int $direction A positive number for ASC, a negative number for DESC
     * @return T[]
     */
    public function sortBy(array $data, string $key, int $direction);
}
