<?php

namespace Monkkey\Tests;

use Monkkey\Tools\Hydrator\ObjectHydrator;

trait CanCreateInstanceTrait
{
    /**
     * @param  array  $objectArray
     * @return object
     */
    public function getHydratedInstance($objectArray)
    {
        $instance = $this->getInstance();
        $objectHydrator = new ObjectHydrator();
        
        return $objectHydrator->hydrate($instance, $objectArray);
    }

    /**
     * @return object
     */
    public function getInstance()
    {
        return new class {
            private $id;
            public $name;
            public function getId()
            {
                return $this->id;
            }
        };
    }
}
