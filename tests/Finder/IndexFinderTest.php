<?php

declare(strict_types=1);

namespace Monkkey\Tests\Finder;

use PHPUnit\Framework\TestCase;
use Monkkey\Tests\CanCreateInstanceTrait;
use Monkkey\Tools\Finder\IndexFinder;

final class IndexFinderTest extends TestCase
{
    use CanCreateInstanceTrait;

    /**
     * @var IndexFinder
     */
    private static $finder = null;

    /**
     * @test
     */
    public function findAll()
    {
        $finder = $this->getFinder();
        $collection = $this->getTestCollection();

        $allIndexes = $finder->findMany($collection, function ($item) {
            return true;
        });
        
        // Get the difference between the output indexes and the input indexes
        $difference = array_diff_key(array_flip($allIndexes), array_keys($collection));
        
        $countMessage = "Searching for indexes with a callback returning true should return an array of the same size";
        $this->assertCount(0, $difference, $countMessage);
    }

    /**
     * @test
     */
    public function findWithoutMatch()
    {
        $finder = $this->getFinder();
        $objectsToSearchIn = $this->getTestCollection();

        $noMatchIndexes = $finder->findMany($objectsToSearchIn, function ($object) {
            return false;
        });

        $countMessage = "Searching for indexes with a callback returning false should return an empty array";
        $this->assertCount(0, $noMatchIndexes, $countMessage);
    }

    /**
     * @test
     */
    public function findMany()
    {
        $finder = $this->getFinder();
        $objectsToSearchIn = $this->getTestCollection();

        $twoFirstIndexes = $finder->findMany($objectsToSearchIn, function ($object) {
            return $object->getId() < 3;
        });

        $this->assertCount(2, $twoFirstIndexes);
        $this->assertSame(0, $twoFirstIndexes[0], "The 1st value should be the index 0 (id = 1)");
        $this->assertSame(1, $twoFirstIndexes[1], "The 2nd value should be the index 1 (id = 2)");

        $indexesOfOddIds = $finder->findMany($objectsToSearchIn, function ($object) {
            return $object->getId() % 2 !== 0;
        });

        $this->assertCount(2, $indexesOfOddIds);
        $this->assertSame(0, $indexesOfOddIds[0], "The 1st value should be the index 0 (id = 1)");
        $this->assertSame(2, $indexesOfOddIds[1], "The 2nd value should be the index 2 (id = 3)");
    }

    /**
     * @return array
     */
    public function getTestCollection()
    {
        return [
            $this->getHydratedInstance([
                "id"   => 1,
                "name" => "Jon",
            ]),
            $this->getHydratedInstance([
                "id"   => 2,
                "name" => "Alfonse",
            ]),
            $this->getHydratedInstance([
                "id"   => 3,
                "name" => "Emmet",
            ]),
            $this->getHydratedInstance([
                "id"   => 4,
                "name" => "Clarence",
            ]),
        ];
    }

    private function getFinder(): IndexFinder
    {
        if (null === static::$finder) {
            static::$finder = new IndexFinder();
        }

        return static::$finder;
    }
}
