<?php
declare(strict_types=1);

namespace Monkkey\Tests\Hydrator;

use PHPUnit\Framework\TestCase;
use Monkkey\Tools\Hydrator\ObjectHydrator;
use Monkkey\Tests\CanCreateInstanceTrait;

final class ObjectHydratorTest extends TestCase
{
    use CanCreateInstanceTrait;

    /**
     * @test
     * @dataProvider dataProvider
     */
    public function publicPropertiesAreSet($id, $name)
    {
        $instance = $this->getHydratedInstance([
            "id"   => $id,
            "name" => $name,
        ]);

        $this->assertEquals($name, $instance->name, "Private properties SHOULD be set");
        $this->assertEquals($name, $instance->name, "Public properties SHOULD be set");
    }

    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [
                1,
                "Jon",
            ],
            [
                2,
                "Alphonse",
            ],
        ];
    }
}
